package com.muhardin.endy.belajar.selenium.pageobject;

public enum PendidikanTerakhir {
    TIDAK_DISEBUTKAN,SD,SMP,SMA,D1,D2,D3,D4,S1,S2,S3
}

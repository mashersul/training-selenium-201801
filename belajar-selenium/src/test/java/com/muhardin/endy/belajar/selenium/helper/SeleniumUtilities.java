package com.muhardin.endy.belajar.selenium.helper;

import java.io.File;
import java.util.UUID;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public abstract class SeleniumUtilities {
    public static void ambilScreenshot(WebDriver webDriver, String judul) {
        try {
            Thread.sleep(3000);
            // String fileScreenshot = "d:\\screenshot\"
            String fileScreenshot = System.getProperty("java.io.tmpdir") 
                    + UUID.randomUUID().toString() + "-" +judul +".png";
            System.out.println("Menyimpan screenshot ke : "+fileScreenshot);
            File scrFile = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
            scrFile.renameTo(new File(fileScreenshot));
        } catch (Exception err){
            err.printStackTrace();
        }
    }
}

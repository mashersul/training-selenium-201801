package com.muhardin.endy.belajar.selenium;

import com.github.javafaker.Faker;
import com.muhardin.endy.belajar.selenium.helper.SeleniumUtilities;
import com.muhardin.endy.belajar.selenium.pageobject.FormEditPelanggan;
import com.muhardin.endy.belajar.selenium.pageobject.JenisKelamin;
import com.muhardin.endy.belajar.selenium.pageobject.PendidikanTerakhir;
import java.net.URL;
import java.text.SimpleDateFormat;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RemoteSeleniumTests {
    private static String URL_HUB = "http://192.168.1.188:4444/wd/hub";
    private static final String URL_APLIKASI = "http://training-selenium.herokuapp.com/pelanggan";
    private static final String PESAN_SUKSES = "Data pelanggan berhasil disimpan";
    private static WebDriver webDriver;
    
    private Faker faker = new Faker();
    
    @BeforeClass
    public static void jalankanBrowser() throws Exception {
        DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
        
        webDriver = new RemoteWebDriver(new URL(URL_HUB), caps);
        webDriver.manage().window().maximize();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test
    public void testInputPelanggan(){
        webDriver.get(URL_APLIKASI+"/form");
        
        // membuat page object untuk form input pelanggan
        FormEditPelanggan form = new FormEditPelanggan(webDriver);
        
        // isi form
        form.isiNama(faker.name().fullName());
        form.isiEmail(faker.name().username()+"@"+faker.internet().domainName());
        form.isiTanggalLahir(new SimpleDateFormat("yyyy-MM-dd").format(faker.date().birthday()));
        form.pilihJenisKelamin(JenisKelamin.WANITA);
        form.pilihPendidikanTerakhir(PendidikanTerakhir.D4);
        form.simpan();
        
        // periksa hasilnya
        Boolean pindahKeHalamanList = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/list"));
        
        Assert.assertTrue(pindahKeHalamanList);
        Assert.assertTrue(webDriver.findElement(By.tagName("body"))
                .getText().contains(PESAN_SUKSES));
        SeleniumUtilities.ambilScreenshot(webDriver, "simpan-normal");
    }
}

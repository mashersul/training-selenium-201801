package com.muhardin.endy.belajar.selenium;

import com.airhacks.sheetfit.ExcelReader;
import com.muhardin.endy.belajar.selenium.helper.SeleniumUtilities;
import java.io.File;
import java.util.Iterator;
import java.util.UUID;
import java.util.function.Function;
import static org.junit.Assert.*;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@RunWith(JUnitParamsRunner.class)
public class PelangganParameterizedTests {
    
    private static final String URL_APLIKASI = "http://training-selenium.herokuapp.com/pelanggan";
    private static final String XPATH_TOMBOL_SIMPAN = "/html/body/form/table/tbody/tr[3]/td[2]/input";
    private static WebDriver webDriver;
    
    @BeforeClass
    public static void jalankanBrowser(){
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test @Parameters(method = "testDataPelanggan")
    public void testInputPelanggan(String skenario, String nama, String email, Boolean sukses, String pesanError, String posisiPesanError) {
        System.out.println("Skenario : "+skenario);
        System.out.println("Nama : "+nama);
        System.out.println("Email : "+email);
        System.out.println("Sukses : "+sukses);
        System.out.println("Pesan error : "+pesanError);
        System.out.println("Posisi error : "+posisiPesanError);
        
        assertNotNull(skenario);
        
        
        webDriver.get(URL_APLIKASI+"/form");
        
        WebElement inputNama = webDriver.findElement(By.name("nama"));
        WebElement inputEmail = webDriver.findElement(By.name("email"));
        WebElement tombolSimpan = webDriver.findElement(By.xpath(XPATH_TOMBOL_SIMPAN));
        
        if(nama != null) {
            inputNama.sendKeys(nama);
        }
        
        if(email != null) {
            inputEmail.sendKeys(email);
        }
        tombolSimpan.click();
        
        if(sukses){
            Boolean pindahKeHalamanList = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/list"));
        
            Assert.assertTrue(pindahKeHalamanList);
            Assert.assertTrue(webDriver.findElement(By.tagName("body"))
                    .getText().contains(pesanError));
        } else {
            Boolean tetapDiHalamanForm = (new WebDriverWait(webDriver, 5))
                    .until(ExpectedConditions.urlContains("pelanggan/form"));

            Assert.assertTrue(tetapDiHalamanForm);

            WebElement webElementError = webDriver.findElement(By.xpath(posisiPesanError));
            Assert.assertNotNull(pesanError);
            Assert.assertEquals(pesanError, webElementError.getText());
        }
        SeleniumUtilities.ambilScreenshot(webDriver, skenario);
    }
    
    public Object[] testDataPelanggan(){
        // argumen : 
        // 1. function untuk map dari cell ke data
        // 2. pakai header atau tidak 
        // 3. jumlah tab
        // 4. nama class, ini sesuai dengan lokasi folder tempat file xlsx berada
        // 5. nama file
        
        // file harus ada di : src/test/resources/pelangganparameterizedtests/test-pelanggan.xlsx
        // source : https://github.com/AdamBien/sheetfit
        return ExcelReader.load(pelangganMapper(), true, 0, this.getClass(), "", "test-pelanggan")
                .toArray();
    }
    
    Function<Row, Object[]> pelangganMapper() {
        return (row) -> {
            Iterator<Cell> cells = row.cellIterator();
            String skenario = row.getCell(0).getStringCellValue();
            String nama = row.getCell(1) != null ? row.getCell(1).getStringCellValue() : null;
            String email = row.getCell(2) != null ? row.getCell(2).getStringCellValue() : null;
            Boolean sukses = row.getCell(3) != null ? row.getCell(3).getBooleanCellValue() : null;
            String pesanError = row.getCell(4) != null ? row.getCell(4).getStringCellValue() : null;
            String lokasiError = row.getCell(5) != null ? row.getCell(5).getStringCellValue() : null;
            return new Object[]{
                    skenario,
                    nama,
                    email,
                    sukses,
                    pesanError,
                    lokasiError};
        };
    }
}

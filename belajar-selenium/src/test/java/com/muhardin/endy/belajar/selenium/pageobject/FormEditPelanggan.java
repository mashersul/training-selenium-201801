package com.muhardin.endy.belajar.selenium.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FormEditPelanggan {
    
    @FindBy(name = "nama")
    private WebElement inputNama;
    
    @FindBy(name = "email")
    private WebElement inputEmail;
    
    @FindBy(name = "tanggalLahir")
    private WebElement inputTanggalLahir;
    
    @FindBy(id = "jenisKelamin1")
    private WebElement radioJenisKelaminTidakDisebutkan;
    
    @FindBy(id = "jenisKelamin2")
    private WebElement radioJenisKelaminPria;
    
    @FindBy(id = "jenisKelamin3")
    private WebElement radioJenisKelaminWanita;
    
    @FindBy(name = "pendidikanTerakhir")
    private WebElement comboPendidikanTerakhir;
    
    @FindBy(id = "simpan")
    private WebElement btnSimpan;
    
    public FormEditPelanggan(WebDriver webDriver){
        PageFactory.initElements(webDriver, this);
    }
    
    public void isiNama(String nama){
        inputNama.sendKeys(nama);
    }
    
    public void isiEmail(String email){
        inputEmail.sendKeys(email);
    }
    
    public void isiTanggalLahir(String tanggal){
        inputTanggalLahir.sendKeys(tanggal);
    }
    
    public void pilihJenisKelamin(JenisKelamin jk){
        if(JenisKelamin.TIDAK_DISEBUTKAN.equals(jk)){
            radioJenisKelaminTidakDisebutkan.click();
        }
        
        if(JenisKelamin.PRIA.equals(jk)){
            radioJenisKelaminPria.click();
        }
        
        if(JenisKelamin.WANITA.equals(jk)){
            radioJenisKelaminWanita.click();
        }
    }
    
    public void pilihPendidikanTerakhir(PendidikanTerakhir pt){
        new Select(comboPendidikanTerakhir).selectByValue(pt.name());
    }
    
    public void simpan(){
        btnSimpan.click();
    }
}
